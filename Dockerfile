#stage 1
FROM node:latest as node
LABEL Name=research-2019-admin-back-end Version=2.2.0
WORKDIR /app
COPY . .
RUN apt-get -y update && apt-get install -y fortunes
RUN npm install
RUN npm run build --prod
#stage 2
FROM nginx:alpine
COPY --from=node dist/ /usr/share/nginx/html/
CMD ["nginx", "-g", "daemon off;"]