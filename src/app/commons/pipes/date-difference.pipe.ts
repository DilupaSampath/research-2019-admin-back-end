import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateDifference'
})
export class DateDifferencePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    const diff = Math.floor(
      new Date(args).getTime() - new Date(value).getTime()
    );
    const day = 1000 * 60 * 60 * 24;

    const days = Math.floor(diff / day);
    const months = Math.floor(days / 31);
    const years = Math.floor(months / 12);

    // final values
    const fYear = years;
    const fMonth = months - fYear * 12;

    return fYear === 0 && fMonth === 0
      ? `${days} Days`
      : `${fYear} ${fYear > 1 ? 'years ' : 'year '} - ${fMonth} ${
          fMonth > 1 ? 'months ' : 'month '
        }`;
  }
}
