import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

/* import service */
import { AuthService } from '../../services/authentication.service';

// import form modules
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { USER_ROLE } from '../../../../assets/js/user-configs';

// import common libraries
import { confPassValidation } from '../../validations/validator';
import { MsgHandelService } from '../../../commons/services/msg-handel.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  // form values
  rForm: FormGroup;

  imageUploadURL: String = `${environment.api_url}uploads`;
  serverImageUploadPath: String = environment.imageUploadPath;
  maxPhotos: Number = 1;
  imageTypes: any = ['jpeg', 'png'];
  photo: String;
  uploadedPhoto: String = null;

  // email regex
  emailPattern: any = /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{1,63}$/;

  // declare constants
  USER_ROLE = USER_ROLE.AGENT;
  hide: Boolean = false;
  hide2: Boolean = false;

  constructor(
    private _AuthService: AuthService,
    private _FormBuilder: FormBuilder,
    private _Router: Router,
    private _MsgHandelService: MsgHandelService
  ) {
    this.rForm = this._FormBuilder.group({
      email: [
        null,
        [
          Validators.required,
          Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)
        ]
      ],
      password: [
        null,
        Validators.compose([Validators.required, Validators.minLength(8)])
      ],
      confirmPassword: [null, [Validators.required, confPassValidation]]
     
    });
  }

  test: Date = new Date();
  ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('register-page');
    body.classList.add('off-canvas-sidebar');
  }
  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('register-page');
    body.classList.remove('off-canvas-sidebar');
  }

  /**
   * Register user
   * @param post : FromGroupValues
   */
  registerUser(post) {
    // create update array
    const userObj = {
      email: post.email,
      password: post.password,
      };

    this._AuthService.registerUser(userObj).subscribe(
      data => {
        if (this._MsgHandelService.handleSuccessResponse(data)) {
          // reload the data
          this._Router.navigate(['/login']);
          // show msg
          this._MsgHandelService.showSuccessMsg(
            'successful!',
            'You have successfully registered with the Live Trains Admin!'
          );
        }
      },
      error => {
        // show msg
        this._MsgHandelService.handleError(error);
      }
    );
  }

  // after upload the photo this function will fire
  onUploadFinished(event) {
    this.uploadedPhoto =
      event.serverResponse.response.body['data']['value']['_id'];
    console.log(this.uploadedPhoto);
  }

  public onRemoved(event) {
    // if the image is removed from the selection
    this.uploadedPhoto = null;
    console.log(this.uploadedPhoto);
  }
}
