import { Injectable } from '@angular/core';

// import common service
import { MainService } from '../../../../infrastructure/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitService {
  constructor(private _apiService: MainService) {}
  loggedUser: string = window.localStorage.getItem('userId');

  getAll(): Observable<any> {
    return this._apiService.get(`api/feedback/all`);
  }

  getOne(): Observable<any> {
    return this._apiService.get(`api/feedback/getOne/${this.loggedUser}`);
  }

  postObjArrvalTime(obj): Observable<any> {
    return this._apiService.post(`googleApi/getDataWithArrivelTime`, obj);
  }
  postObjNearByPlaceMapRequest(obj): Observable<any> {
    return this._apiService.post(`googleApi/getNearByPlaces`, obj);
  }

  validatePoint(obj): Observable<any> {
    return this._apiService.post(`googleApi/validatePoint`, obj);
  }

  putObj(obj): Observable<any> {
    return this._apiService.patch(
      `api/feedback//update/${this.loggedUser}`,
      obj
    );
  }

  deleteObj(id): Observable<any> {
    return this._apiService.delete(`api/feedback/remove/${id}`);
  }
}
