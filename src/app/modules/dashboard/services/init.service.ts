import { Injectable } from '@angular/core';

// import common service
import { Observable } from 'rxjs';
import { MainService } from '../../../infrastructure/api.service';

// import env file to get the sever end point
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InitService {
  constructor(private _MainService: MainService) {}

  // get all dealers
  getAllDealers(): Observable<any> {
    return this._MainService.get(`users/all/dealers`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // get all sales stores
  getAllStores(): Observable<any> {
    return this._MainService.get(`sale-store/all`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // get all sales stores For agent
  getSalesForAgent(agentId): Observable<any> {
    return this._MainService.get(`sale-store/dealer/${agentId}`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // get registered agent count
  getAgentCount(): Observable<any> {
    return this._MainService.get(`users/count?role=agent&registered=true`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // get sale count
  getSaleCount(id): Observable<any> {
    return this._MainService.get(`sale-store/count?id=${id}`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // get regitered vehicle count
  getRegisteredVehicleCount(id): Observable<any> {
    return this._MainService.get(`vehicle/count?id=${id}&registered=true`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // get unregitered vehicle count
  getUnRegisteredVehicleCount(id): Observable<any> {
    return this._MainService
      .get(`vehicle/count?id=${id}&registered=false`)
      .pipe(
        map(data => {
          return data;
        }),
        catchError(res => {
          throw res;
        })
      );
  }

  // get profit
  getProfit(id): Observable<any> {
    return this._MainService.get(`vehicle-booking/profit?id=${id}`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // create new dealer
  approveDealer(body): Observable<any> {
    return this._MainService.patch(`dealers/verify`, body).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // Monthly Order Count
  monthlyOrders(id): Observable<any> {
    return this._MainService.get(`vehicle-booking/monthly?id=${id}`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // trending Data
  getTrendingData(param): Observable<any> {
    return this._MainService.get(`vehicle-booking/trending/${param}`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // popular brands
  getPopularBrands(): Observable<any> {
    return this._MainService.get(`vehicle-booking/popular-brands`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // get all rating
  allRating(): Observable<any> {
    return this._MainService.get(`agent-rating/all`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }

  // get agent rating
  allAgentRating(id): Observable<any> {
    return this._MainService.get(`agent-rating/all?dealerId=${id}`).pipe(
      map(data => {
        return data;
      }),
      catchError(res => {
        throw res;
      })
    );
  }
}
