import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from './md/md.module';
import { AppMaterialModule } from '../../app.material.module';
import { environment } from '../../../environments/environment';

/**
 * import module
 */
import { NgxEchartsModule } from 'ngx-echarts';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    NgxEchartsModule,
    RouterModule.forChild(DashboardRoutes),
    FormsModule,
    MdModule,
    AgmCoreModule.forRoot({
      apiKey: environment.apiKey,
      libraries: ['places']
    }),
    AppMaterialModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule {}
