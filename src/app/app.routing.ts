import { Routes } from '@angular/router';

/* components */
import { LoginComponent } from './commons/components/login/login.component';
import { RegisterComponent } from './commons/components/register/register.component';
import { AdminLayoutComponent } from './commons/layouts/admin/admin-layout.component';

/* AuthGuard */
import { AuthGuardService } from './infrastructure/auth-guard.service';
import { AuthRouteGuardService } from './commons/services/authorization-route-guard.service';

/* Router List */
export const AppRoutes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  // when after login to the system at the first time , this route will be called
  {
    path: '',
    redirectTo: 'dashboard',
    canActivate: [],
    pathMatch: 'full'
  },
  // lazy loaded modules
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [],
    children: [
      {
        path: 'dashboard',
        canActivate: [],
        loadChildren: './modules/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'locations',
        canActivate: [],
        loadChildren: './modules/example/custom.module#CustomModule'
      }
      ,
      {
        path: 'train-stations',
        canActivate: [],
        loadChildren: './modules/train-stations/trainStations.module#TrainStationsModule'
      },
      {
        path: 'trains',
        canActivate: [],
        loadChildren: './modules/trains/trains.module#TrainsModule'
      },
      {
        path: 'time-tables',
        canActivate: [],
        loadChildren: './modules/time-tables/timeTables.module#timeTablesModule'
      },
      {
        path: 'api-test',
        canActivate: [],
        loadChildren: './modules/api-test/api-test.module#ApiTestModule'
      },
      {
        path: 'game',
        canActivate: [],
        loadChildren: './modules/game/game.module#GameModule'
      }
    ]
  }
];
