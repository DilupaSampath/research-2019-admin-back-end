export const ADMIN_ROUTES: RouteInfo[] = [
  {
    path: '/dashboard',
    title: 'Dashboard',
    type: 'link',
    icontype: 'dashboard'
  },
  {
    path: '/locations',
    title: 'Locations',
    type: 'link',
    icontype: 'dashboard'
  },{
    path: '/train-stations',
    title: 'Train Stations',
    type: 'link',
    icontype: 'dashboard'
  },
  {
    path: '/trains',
    title: 'Trains',
    type: 'link',
    icontype: 'dashboard'
  },
  {
    path: '/time-tables',
    title: 'Time Tables',
    type: 'link',
    icontype: 'dashboard'
  },
  // {
  //   path: '/bookings',
  //   title: 'Booking',
  //   type: 'sub',
  //   icontype: 'shopping_cart',
  //   collapse: 'bookings',
  //   children: [
  //     { path: 'add-booking', title: 'Add', ab: 'A' },
  //     { path: 'booking-request', title: 'Requests', ab: 'R' }
  //   ]
  // },
  // {
  //   path: '/analytics',
  //   title: 'Analytics',
  //   type: 'sub',
  //   icontype: 'trending_up',
  //   collapse: 'Analytics',
  //   children: [{ path: 'predictions', title: 'Predictions', ab: 'PR' }]
  // },
  // {
  //   path: '/vehicle-management',
  //   title: 'Vehicle Management',
  //   type: 'sub',
  //   icontype: 'commute',
  //   collapse: 'Vehicle Management',
  //   children: [
  //     { path: 'listed-vehicles', title: 'View All', ab: 'VA' },
  //     { path: 'vehicle-register', title: 'Add', ab: 'A' },
  //     { path: 'pending-vehicles', title: 'Requests', ab: 'RE' }
  //   ]
  // },
  // {
  //   path: '/dealer-management',
  //   title: 'Dealer Management',
  //   type: 'sub',
  //   icontype: 'people',
  //   collapse: 'Dealer Management',
  //   children: [
  //     { path: 'info', title: 'View All', ab: 'VA' },
  //     { path: 'requests', title: 'Requests', ab: 'RE' }
  //   ]
  // },
  // {
  //   path: '/sales',
  //   title: 'Branch Management',
  //   type: 'sub',
  //   icontype: 'business',
  //   collapse: 'Branch Management',
  //   children: [{ path: 'view-sales', title: 'View All', ab: 'VA' }]
  // },

  {
    path: '/api-test',
    title: 'ApiTest',
    type: 'link',
    icontype: 'add_alert'
  },
  {
    path: '/example',
    title: 'Configurations',
    type: 'link',
    icontype: 'settings'
  },
  {
    path: '/game',
    title: 'Train Game',
    type: 'link',
    icontype: 'settings'
  }
];

export const AGENT_ROUTES: RouteInfo[] = [
  {
    path: '/dashboard',
    title: 'Dashboard',
    type: 'link',
    icontype: 'dashboard'
  },
  {
    path: '/bookings',
    title: 'Booking',
    type: 'sub',
    icontype: 'shopping_cart',
    collapse: 'bookings',
    children: [
      { path: 'add-booking', title: 'Add', ab: 'A' },
      { path: 'booking-request', title: 'Requests', ab: 'R' }
    ]
  },
  {
    path: '/analytics',
    title: 'Analytics',
    type: 'sub',
    icontype: 'trending_up',
    collapse: 'Analytics',
    children: [{ path: 'predictions', title: 'Predictions', ab: 'PR' }]
  },
  {
    path: '/vehicle-management',
    title: 'Vehicle Management',
    type: 'sub',
    icontype: 'commute',
    collapse: 'Vehicle Management',
    children: [
      { path: 'listed-vehicles', title: 'View All', ab: 'VA' },
      { path: 'vehicle-register', title: 'Add', ab: 'A' }
    ]
  },
  {
    path: '/sales',
    title: 'Sales Management',
    type: 'sub',
    icontype: 'business',
    collapse: 'Branch Management',
    children: [
      { path: 'view-sales', title: 'View All', ab: 'VA' },
      { path: 'add-sales', title: 'Add', ab: 'A' }
    ]
  }
];

export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  icontype: string;
  collapse?: string;
  children?: ChildrenItems[];
}

export interface ChildrenItems {
  path: string;
  title: string;
  ab: string;
  type?: string;
}
